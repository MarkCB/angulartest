﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>
    <script src="<%=ResolveClientUrl("~/Shared/Js/TestController.js") %>"></script>
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>
</head>
<body ng-app>
    <div ng-controller="testController">
        Hi this is the index view.
        <br />
        <input type="text" ng-model="person.name" />
        <br />
        <input type="text" ng-model="person.surname" />
        <br />
        <input id="Button1" type="button" value="Send data to controller" ng-click="sendDataToController()"/><br />
        <input id="Button1" type="button" value="Send post data to web API" ng-click="sendPostToApi()"/><br />
        <input id="Button1" type="button" value="Send put data to web API" ng-click="sendPutToApi()"/><br />
        <input id="Button1" type="button" value="Send delete data to web API" ng-click="sendDeleteToApi()"/><br />
        <div>The name is: {{person.name}}</div><br />
    </div>
</body>
</html>
