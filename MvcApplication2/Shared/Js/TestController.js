﻿function testController($scope, $http) {

    $scope.person = {name:"", surname:""}

    $scope.sendDataToController = function () {
        $http.post('Home/JsonRequest', { "name": $scope.person.name, "Surname": $scope.person.surname }).
            success(function (data) { alert("The result code is: " + data.Code); });
    }

    $scope.sendPostToApi = function () {        
        $http.post('api/person', $scope.person).success(
            function (data) { alert("Request sent"); }).error(
            function (data) { alert("Error" + data); });
    }

    $scope.sendPutToApi = function () {
        $http.put('api/person/1', $scope.person).success(
            function (data) { alert("Request sent"); }).error(
            function (data) { alert("Error" + data); });
    }

    $scope.sendDeleteToApi = function () {
        $http.delete('api/person/1').success(
            function (data) { alert("Request sent"); }).error(
            function (data) { alert("Error" + data); });
    }
}