﻿using MvcApplication2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication2.Controllers
{
    public class PersonController : ApiController
    {
        // GET api/person
        public IEnumerable<Person> Get()
        {
            return new Person[] { };
        }

        // GET api/person/5
        public Person Get(int id)
        {
            return new Person();
        }

        // POST api/person
        public void Post([FromBody]Person value)
        {
            ;
        }

        // PUT api/person/5
        public void Put(int id, [FromBody]Person value)
        {
            ;
        }

        // DELETE api/person/5
        public void Delete(int id)
        {
            ;
        }
    }
}
