﻿using MvcApplication2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication2.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(string someData)
        {
            if (someData == "2")
                return View("~/Views/Home/Index2.aspx");

            return View();
        }

        public ActionResult TestJson()
        {
            Person p = new Person()
            {
                Name = "Marc",
                Surname = "Cortada"
            };
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult JsonRequest(Person p)
        {
            return Json(new Result() {Code = 1, Message = "OK"});
        }
    }
}
